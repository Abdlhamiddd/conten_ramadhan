<!DOCTYPE html>
<html>

<head>
    <title>Menu Berbuka Puasa</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
    <div class="container">
        <h1 class="card">Menu Berbuka Puasa</h1>
    </div>
    <div class="container">
        <div class="card">
            <?php
            if (isset($_POST['nama']) && isset($_POST['menu'])) {
                $nama = $_POST['nama'];
                $menu = $_POST['menu'];
            ?>
                <p>Selamat berbuka puasa, <?= $nama ?>!</p>
                <p>Berikut adalah pilihan menu berbuka puasa Anda:</p>
                <ul>
                    <?php foreach ($menu as $m) { ?>
                        <li><?= $m ?></li>
                    <?php } ?>
                </ul>
                <a class="button" href="index.php">Kembali</a>
            <?php } else {
                header("Location: index.php");
            } ?>
        </div>
    </div>
</body>

</html>