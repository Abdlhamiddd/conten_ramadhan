<!DOCTYPE html>
<html>

<head>
    <title>Menu Berbuka Puasa</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
    <div class="container">
        <h1 class="card">Menu Berbuka Puasa</h1>
    </div>
    <form class="container" action="menu.php" method="post">
        <div class="card">
            <label>Masukkan Nama Anda:</label>
            <input class="input" type="text" name="nama" required><br>
            <label>Pilih Menu Berbuka Puasa:</label><br>
            <input type="checkbox" name="menu[]" value="Kurma"> Kurma<br>
            <input type="checkbox" name="menu[]" value="Es Buah"> Es Buah<br>
            <input type="checkbox" name="menu[]" value="Sop"> Sop<br>
            <input type="checkbox" name="menu[]" value="Ketupat"> Ketupat<br>
            <input type="checkbox" name="menu[]" value="Lontong Sayur"> Lontong Sayur<br><br>
            <button class="button" type="submit">Submit</button>
        </div>
    </form>
</body>

</html>